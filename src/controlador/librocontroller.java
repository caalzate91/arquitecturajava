package controlador;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Categoria;
import modelo.Libro;

/**
 * Servlet implementation class librocontroller
 */
@WebServlet("/librocontroller")
public class librocontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public librocontroller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher despachador = null;
		if(request.getParameter("idlibro")!=null){
			int isbn = Integer.valueOf(request.getParameter("idlibro"));
			Libro libro = new Libro();
			libro.eliminar(isbn);
		}
		if(request.getParameter("idEditar")!=null){
			int isbn = Integer.valueOf(request.getParameter("idEditar"));
			Libro libro = Libro.buscarPorId(isbn);
			List<Categoria> listaCategoria = Categoria.buscarTodos();
			request.setAttribute("libro",libro );
			request.setAttribute("listaDeCategorias", listaCategoria);
			despachador = request.getRequestDispatcher("libroEditar.jsp");
			despachador.forward(request, response);
		}
		
		
		List<Libro> listaLibros = Libro.buscarTodos();
		request.setAttribute("listaDeLibros", listaLibros);
		despachador = request.getRequestDispatcher("index.jsp");
		despachador.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 if (request.getParameter("btnguardar")!=null)
         {
           
			// TODO Auto-generated method stub
				RequestDispatcher despachador = null;
				int isbn = Integer.valueOf(request.getParameter("isbn"));
				String titulo = request.getParameter("titulo");
				int categoriaId = Integer.valueOf(request.getParameter("categoria"));
				Categoria categoria = new Categoria(categoriaId);
				Libro libro = new Libro(isbn,titulo,categoria);
				libro.insertar();
				List<Libro> listaLibros = Libro.buscarTodos();
				request.setAttribute("listaDeLibros", listaLibros);
				request.setAttribute("mensaje", "se inserto el libro correctamente");
				despachador = request.getRequestDispatcher("index.jsp");
				despachador.forward(request, response);
         }
		 
		 if (request.getParameter("btneditar")!=null)
         {
           
		 	RequestDispatcher despachador = null;
			int isbn = Integer.valueOf(request.getParameter("isbn"));
			String titulo = request.getParameter("titulo");
			int categoriaId = Integer.valueOf(request.getParameter("categoria"));
			Categoria categoria = new Categoria(categoriaId);
			Libro libro = new Libro(isbn,titulo,categoria);
			libro.salvar();
			List<Libro> listaLibros = Libro.buscarTodos();
			request.setAttribute("listaDeLibros", listaLibros);
			request.setAttribute("mensaje", "se actualizo el libro correctamente");
			despachador = request.getRequestDispatcher("index.jsp");
			despachador.forward(request, response);
         }
	}
	
	protected void editar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher despachador = null;
		
		despachador = request.getRequestDispatcher("exito.jsp");
		despachador.forward(request, response);
	}
	
	

}
