package modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


/**
 * Bean que representa a la tabla 'categorias' de la BB.DD.
 * Relacci�n 1-N con la tabla 'libros'.
 * Clave principal: id
 * 
 * @author eladio
 */
@Entity
@Table(name="categoria")
public class Categoria {

	@Id
	private int id_categoria;
	

	private String descripcion;
	@OneToMany
	@JoinColumn(name="id_categoria")
	private List<Libro> listaDeLibros;
	
	// Constructores
	
	public Categoria() {
	}
	
	public Categoria(int id) {
		this.id_categoria = id;
	}
	
	public Categoria(int id, String nombre) {
		this.id_categoria = id;
		this.descripcion = nombre;
	}
	
	// Getters y Setters
	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public List<Libro> getListaDeLibros() {
		return this.listaDeLibros;
	}
	
	
	public void setDescripcion(String nombre) {
		this.descripcion = nombre;
	}
	
	public void setListaDeLibros(List<Libro> listaDeLibros) {
		this.listaDeLibros = listaDeLibros;
	}
	
	public static List<Categoria> buscarTodos() {
		SessionFactory factoriaSession = HibernateHelper.getSessionFactory();
		Session session = factoriaSession.openSession();
		List<Categoria> listaDeCategorias = session.createQuery(" from Categoria categoria").list();
		session.close();
		return listaDeCategorias;
		
		
	}
	
}
