<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="modelo.Libro" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inicio</title>
</head>
<body>

	<%
		if (request.getAttribute("mensaje") != null) {
			out.println(request.getAttribute("mensaje"));
		}

		if (request.getAttribute("listaDeLibros") != null) {
			%>
			<table>
				<tr>
					<th>isbn</th>
					<th>titulo</th>
					<th>categoria</th>
					<th>Acciones</th>
				</tr>
				
			
			<c:forEach var="libro" items="${listaDeLibros}">
			<tr>
			<td>${libro.isbn}</td>
			<td>${libro.titulo}</td>
			<td>${libro.categoria.descripcion}</td>
			<td>
				<a href="librocontroller?idlibro=${libro.isbn}">borrar</a>--
				<a href="librocontroller?idEditar=${libro.isbn}">editar</a>
			</td>
			</tr>
			
			
			<br />
			</c:forEach>
			</table>
			<%
		}
	%>

	<a href="libroView.jsp">Insertar libro</a>
	<a href="librocontroller">listar libro</a>
</body>
</html>