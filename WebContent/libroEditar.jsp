<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form id="formeditar" action="librocontroller" method="post">
		<fieldset>
		<legend>Formulario edición libro</legend>
			<p>
				<label for="isbn">ISBN:</label>
				<input id="isbn" type="text" name="isbn" value="${libro.isbn}" >
			</p>
			
			<p>
				<label for="titulo">Titulo:</label>
				<input id="titulo" type="text" name="titulo" value="${libro.titulo}">
			</p>
			
			<p>
				<label for="categoria">Categoria:</label>
				<select name="categoria">
					<c:forEach var="categoria" items="${listaDeCategorias}">
						<option value="${categoria.id_categoria}">${categoria.descripcion}</option>
					</c:forEach>
				</select>
			</p>
			
			<p>
				<input type="submit" name="btneditar" value="Salvar">
			</p>
		</fieldset>
	</form>
</body>
</html>